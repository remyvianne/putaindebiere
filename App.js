/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useEffect } from 'react';
import type { Node } from 'react';
import { SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, useColorScheme, View, TouchableOpacity, TextInput, } from 'react-native';
import { Colors, DebugInstructions, Header, LearnMoreLinks, ReloadInstructions } from 'react-native/Libraries/NewAppScreen';
import ReactNativeBiometrics from 'react-native-biometrics';
import AsyncStorage from '@react-native-async-storage/async-storage';


const App: () => Node = () => {

  const isDarkMode = useColorScheme() === 'dark';
  const backgroundStyle = { backgroundColor: isDarkMode ? Colors.darker : Colors.lighter };

  const [nameLogin, setNameLogin] = useState('');
  const [surnameLogin, setSurnameLogin] = useState('');

  const [nameRegister, setNameRegister] = useState('');
  const [surnameRegister, setSurnameRegister] = useState('');

  const [styleHidden, setStyleHidden] = useState(true);


  /**
   * Vérifie si dans le LocalStorage le prénom et nom ont une valeur
   * @param {string} key 
   * @returns 
   */
  const isDataNull = async (key) => {
    try {
      const value = await AsyncStorage.getItem(key);
      return !(value !== null)
    } catch (e) {
      // error reading value
      return true;
    }

  }

  /**
   * Enregistre clé/valeur passé en paramètre dans le local storage
   * @param {string} key 
   * @param {string} value 
   */
  const storeData = async (key, value) => {
    try {
      await AsyncStorage.setItem(key, value)
      alert("Données enregistées avec succès")
      console.log(key + " : " + value)
    } catch (e) {
      // saving error
      alert(e);
    }
  }

  /**
   * Récupère la valeur grâce à la clé en paramètre
   * Assigne la donnée récupéré au champ correspondant
   * @param {string} key 
   */
  const getData = async (key) => {
    try {
      const value = await AsyncStorage.getItem(key)
      if (value !== null) {
        // Test biometrique
        if (key == "name") { setNameLogin(value); }
        else if (key == "surname") { setSurnameLogin(value); }
      } else {
        alert("Pas de données pour la clé " + key);
      }

    } catch (e) {
      // error reading value
    }
  }

  /**
   * Reset le Local Storage, les champs et désaffiche les champs du login
   */
  const clearStorage = async () => {
    try {
      await AsyncStorage.clear();
      setNameLogin('');
      setSurnameLogin('');
      setNameRegister('');
      setSurnameRegister('');
      setStyleHidden(true);
      alert('Stockage et champs effacés');
    } catch (e) {
      alert('Erreur lors de la suppression du stockage');
    }
  };

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={backgroundStyle}>
        <View
          style={{
            backgroundColor: isDarkMode ? Colors.black : Colors.white,
          }}><View title="Reset">
            <Text style={styles.titre}>Login</Text>
            <TextInput
              placeholder='Prénom'
              value={nameLogin}
              onChangeText={(text) => setNameLogin(text)}
              style={styleHidden ? styles.hidden : styles.input}
            />
            <TextInput
              placeholder='Nom'
              value={surnameLogin}
              onChangeText={(text) => setSurnameLogin(text)}
              style={styleHidden ? styles.hidden : styles.input}
            />
            {/* Bouton quand on appuie, affiche une alerte "test" */}
            <TouchableOpacity
              style={styles.button}
              onPress={async () => {
                if (!(await isDataNull("name")) & !(await isDataNull("surname"))) {
                  if (await testBiometric()) {
                    setStyleHidden(false);
                    getData("name");
                    getData("surname");
                  }
                } else {
                  alert("Veuillez remplir TOUS les champs dans register avant de vous connecter");
                }
              }}>
              <Text>Se connecter</Text>
            </TouchableOpacity>
          </View>
          <View title="Register">
            <Text style={styles.titre}>Register</Text>
            <TextInput
              placeholder='Prénom'
              value={nameRegister}
              onChangeText={setNameRegister}
              style={styles.input}
            />
            <TextInput
              placeholder='Nom'
              value={surnameRegister}
              onChangeText={setSurnameRegister}
              style={styles.input}
            />
            <TouchableOpacity
              style={styles.button}
              onPress={async () => {
                if (nameRegister != '' && surnameRegister != '') {
                  if (await testBiometric()) {
                    await storeData("name", nameRegister)
                    await storeData("surname", surnameRegister)
                  }
                } else {
                  alert("Veuillez remplir TOUS les champs");
                }
              }}
            >
              <Text>S'enregistrer</Text>
            </TouchableOpacity>
          </View>
          <View title="Reset">
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                clearStorage();
              }}
            >
              <Text>Reset localStorage et champs</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView >
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
  titre: {
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 20,
  },
  button: {
    backgroundColor: '#0066ff',
    padding: 10,
    margin: 12,
    borderWidth: 1,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  hidden: {
    display: 'none'
  }
});

/**
 * Test si l'appareil possède une biometrique, faceID ou FingerPrint
 * une fois la promise terminé, retourne un string FingerPrint || FaceID || TouchID || None
 * @returns {Promise}
 */
async function testBiometric() {
  const isSupported = await ReactNativeBiometrics.isSensorAvailable();
  const { available, biometryType } = isSupported;
  if (!available) {
    alert("Biometrics pas supporté");
  }
  if (available && biometryType === ReactNativeBiometrics.TouchID) {
    console.log("TouchID ok");
  } else if (available && biometryType === ReactNativeBiometrics.FaceID) {
    console.log("FaceID");
  } else if (available && biometryType === ReactNativeBiometrics.Biometrics) {
    console.log("FingerPrint");
    return ReactNativeBiometrics.simplePrompt({ promptMessage: 'Confirmez votre empreinte' })
      .then((resultObject) => {
        const { success } = resultObject;
        if (!success) {
          alert("Empreinte annulée");
        }
        return success;
      })
      .catch(() => {
        alert('Erreur lors de la confirmation de l\'empreinte');
      })
  }
  return false;
}

export default App;